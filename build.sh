#!/bin/bash
docker run -u root -v "$PWD":/home/gradle/project -w /home/gradle/project  gradle:7.4.2-jdk11  gradle --no-daemon clean assemble
docker run -v "$PWD:/usr/src/camelpack" -w /usr/src/camelpack node:8 bash -c 'npm install && npm run build'
