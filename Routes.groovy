i = 0
router = {

  from("activemq:example.A")
      .transform { e,c ->  e.in.body.reverse() + " " + (i++) }
      .to("activemq:example.B")
      .to("activemq:example.C")
      .to("activemq:example.D")
          /*
  from("activemq:example.D")
      .transform { e,c ->  e.in.body.reverse() }
      .to("activemq:example.B")
      */
}

/*
router = {
  // from("activemq:example.A").multicast().to("activemq:example.C","activemq:example.D")
  from("activemq:example.A")
      .to("activemq:example.C")
      .transform { e,c -> println(e.in.body); e.in.body.reverse() }
      .to("activemq:example.D")
      .to("xmpp://talk.google.com:5222/ssadedin@gmail.com?serviceName=gmail.com&user=simonzmail@gmail.com&password=t3st@cc0unt")
      .process { e -> println "Finally finished processing : " + e.in.body }

  // from("file:///tmp/queue") to("file:///tmp/queueout")

  from("file:///tmp/queue").to("xmpp://talk.google.com:5222/ssadedin@gmail.com?serviceName=gmail.com&user=simonzmail@gmail.com&password=t3st@cc0unt")
}
*/
