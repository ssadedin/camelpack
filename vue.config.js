module.exports = {
  outputDir: 'build/js',
  devServer: {
      proxy: {
        "/route": {
          target: "http://localhost:7775"
        }
      }
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@': __dirname + '/src/main/js'
      }
    },
    entry: {
      app: './src/main/js/main.js'
    }
  }
}
