FROM eclipse-temurin:11

ARG INCLUDE_HAWTIO="false"
ARG HAWTIO_VERSION="2.17.6"

COPY README* *build.d /build.d/
RUN bash -c 'for i in $(ls /build.d/*.sh 2>/dev/null | sort) ; do source $i ; done'

RUN mkdir build

RUN sh -c '${INCLUDE_HAWTIO} && { mkdir -p hawtio && cd hawtio && wget https://repo1.maven.org/maven2/io/hawt/hawtio-default/${HAWTIO_VERSION}/hawtio-default-${HAWTIO_VERSION}.war; } || true'

COPY *build/js build/js
COPY build/libs/camelpack-*-all.jar camelpack.jar
COPY hawtio* LICENSE  hawtio/
RUN chmod -R 777 /hawtio
RUN chown -R root:root  /hawtio
CMD java -Djava.naming.factory.initial=org.apache.camel.util.jndi.CamelInitialContextFactory ${JAVA_OPTS} -jar camelpack.jar
