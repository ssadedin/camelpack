import { format } from 'date-fns'

export default {
    
    log(msg,obj) {
        if(obj) {
            window.console.log(msg + ' [' + typeof(obj) + ']:')
            window.console.log(obj)
        }
        else {
            window.console.log(msg)
        }
    },
    
    formatDate(d) {
      return format(d, 'YYYY-MM-DD')
    },
    
    formatDateTime(d) {
      let result =  format(d, 'YYYY-MM-DD HH:mm:ss')
      return result.replace(/00:00:00$/,'')
    }, 
    
    ucfirst(value) {
     return value[0].toUpperCase() + value.substring(1).toLowerCase()
    },
    
    fixedDigits(value, digits) {
        if(typeof(value) == 'undefined')
            return ''
        else
            return parseFloat(value.toFixed(digits).replace(/[.]{0,1}0*$/,''))
    },
    
    encode(value) {
      return encodeURIComponent(value)
    }, 
    
    partial(fn, ...args1) {
      return (...args2) => fn(...args1, ...args2);
    },
    
    /**
     * Defines a handler for the escape key that is called exaclty once if `esc` is pressed
     * Intended to dismiss modal dialogs etc.
     * Needed because VueJS can't handle events on non-focusable elements
     */
    esc(fn) {
        let handler = (evt) => {
            if(evt.keyCode === 27) {
                document.removeEventListener('keyup',handler, true)
                fn()
            }
        }
        document.addEventListener('keyup', handler, true);
    },
    
    groupBy(xs, fn) {
        return xs.reduce(function (acc, val) {
            let key = fn(val);
            (acc[key] = acc[key] || []).push(val);
            return acc;
        }, {});
    },

    isWindows() {
        return navigator.platform.indexOf('Win') > -1
    },

    isMac() {
        return navigator.platform == 'MacIntel'
    },
}
  