package camelpack

import org.apache.camel.Exchange
import org.apache.camel.Expression
import org.apache.camel.builder.ExpressionClause
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.builder.SimpleBuilder
import org.apache.camel.builder.ValueBuilder
import org.apache.camel.component.file.GenericFile
import org.apache.camel.impl.SimpleRegistry
import org.apache.camel.model.AggregateDefinition
import org.apache.camel.model.InterceptDefinition
import org.apache.camel.model.InterceptFromDefinition
import org.apache.camel.model.OnExceptionDefinition
import org.apache.camel.model.rest.RestBindingDefinition
import org.apache.camel.model.rest.RestConfigurationDefinition
import org.apache.camel.model.rest.RestDefinition
import org.apache.camel.processor.aggregate.AggregationStrategy
import org.slf4j.Logger

import groovy.text.Template

/**
 * A dummy class designed as a pseudo-interface to enable IDE support for camelpack routes
 * 
 * @author Simon Sadedin
 */
abstract class RouteBuilderScript extends Script {
    
    Map<String,Object> globals

    ConfigObject config
    
    Map<String,Object> getGlobals() {  }
    
    ConfigObjectWithEnv getConfig() {  }

    String toJSON(Object value) { }
    
    org.apache.camel.model.RouteDefinition from(Object... args) {
    }
    
    org.apache.camel.model.OnExceptionDefinition onException(Throwable... args) {
    }

    org.apache.camel.model.RouteDefinition when(Closure c) {
    }
    
    ExpressionClause<AggregateDefinition> aggregate() { }

    ExpressionClause<AggregateDefinition> aggregate(AggregationStrategy aggregationStrategy) { }

    ExpressionClause<AggregateDefinition> aggregate(Expression correlationExpression) { }

    ExpressionClause<AggregateDefinition> aggregate(Expression correlationExpression,
                                     AggregationStrategy aggregationStrategy) { }
    
    Logger getLog() {
        
    }
    
    Object parseJSON(Exchange e) { }

    Object parseJSON(String e) { }

    Object parseJSON(File e) { }

    Object parseJSON(Reader e) { }

    Object parseJSON(GenericFile e) { }
    
    SimpleRegistry getRegistry() { }
   
    ValueBuilder header(String name) { }
    ValueBuilder exchangeProperty(String name) { }
    ValueBuilder constant(Object value) { }
    ValueBuilder systemProperty(String name) { }
    ValueBuilder jsonpath(String path) { }
    ValueBuilder body() { }
    ValueBuilder bodyAs(Class c) { }
    
    SimpleBuilder simple(String value) { }
    
    RestDefinition rest(String urlPath) { }
    
    RestConfigurationDefinition restConfiguration() { }

    InterceptDefinition intercept() { }
    InterceptFromDefinition interceptFrom() { }
    OnExceptionDefinition onException(Class<?>... classes) {}
    
    Map<String,Closure> getTemplates() { }
    
    void require(String routeName) { }
    
    String stacktrace(Exchange e) { }
}

class ConfigObjectWithEnv extends ConfigObject {
    
    static <T> T env(String key, T defaultValue, Class<T> type=null) {
        
    }
}
