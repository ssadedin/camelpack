package camelpack

import java.util.logging.ConsoleHandler
import java.util.logging.Level
import java.util.logging.Logger

import org.apache.camel.impl.DefaultExchange

import groovy.util.logging.Log4j
import io.micronaut.runtime.Micronaut

@Log4j
class Application {
    
    static JSONHelpers jsonHelpers = new JSONHelpers()
    
    static void main(String[] args) {
        
        configureSimpleLogging()
        
        if(new File('hawtio').exists()) {
            File warFile = new File('hawtio').listFiles().findAll { it.name.endsWith('.war') }.sort { -it.lastModified() }[0]
            if(warFile) {
                String activemqHost = System.getProperty('activemq.host', 'activemq')
                System.setProperty('hawtio.proxyAllowlist',activemqHost)
                log.info "Found war file for hawtio: launching interface ..."
                System.setProperty("hawtio.authenticationEnabled", "false");
                def main = new io.hawt.embedded.Main();
                main.setWar(warFile.absolutePath);
                main.run();
            }
        }
        
        initConversions()
       
        Micronaut.run(Application)
    }
    
    public static void initConversions() {
         DefaultExchange.metaClass.asType << { Class clazz ->
            // Due to differing classloaders, we have to use class name
            // rather than actual class
            // TODO: should look further up class hierachy
            if(clazz.superclass.name == 'camelpack.Model' || clazz.superclass?.superclass.name == 'camelpack.Model') {
                Map body
                if(delegate instanceof Map)
                    body = delegate
                else
                if(delegate.in.body?.class == clazz) {
                    return delegate.in.body
                }
                else
                    body = jsonHelpers.parseJSON(delegate.in.body)
                return clazz.newInstance([body] as Object[])
            }
        }
    }
    
    /**
     * Set up Java logging with a nice, simple, reasonable format
     */
    public static void configureSimpleLogging(Level level = Level.INFO) {
        ConsoleHandler console = new ConsoleHandler()
        console.setFormatter(new SimpleLogFormatter())
        console.setLevel(level)
        Logger log = Logger.getLogger("dummy")
        def parentLog = log.getParent()
        parentLog.getHandlers().each { parentLog.removeHandler(it) }
        log.getParent().addHandler(console)
        
        // Disable logging from groovy sql
        Logger.getLogger("groovy.sql.Sql").useParentHandlers = false

    }
}
