package camelpack

import org.apache.camel.builder.RouteBuilder

abstract class RouteBuilderScriptBase extends Script {
    
    @Delegate
    RouteBuilder routeBuilder
    
    RouteBuilderScriptBase() {
    }
}
