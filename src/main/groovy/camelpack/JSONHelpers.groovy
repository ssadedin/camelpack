package camelpack

import groovy.json.*

import org.apache.camel.Exchange
import org.apache.camel.component.file.GenericFile

class JSONHelpers {
    Object parseJSON(Object value) {
        def parser = new JsonSlurperClassic()
        if(value instanceof Exchange) {
            value = value.in.body
        }
        if(value instanceof byte[]) {
            value = new String(value)
        }

        if(value instanceof GenericFile) {
            value = value.file.text
        }
        else
        if(value instanceof File || value instanceof Reader || value instanceof InputStream) {
            value = value.text
        }
        
        if(value instanceof String)
            return parser.parseText(value)
        else
            return value
    }
    
    String toJSON(Object value) {
        if(value instanceof Exchange)
            value = value.out.body

        if(value instanceof Model) {
            def generator = new JsonGenerator.Options()
                .excludeFieldsByName('undeclared', 'thisReader', 'quietProperties')
                .build()

            return new JsonBuilder(value, generator).toPrettyString()
        }
        else {
            JsonOutput.prettyPrint(JsonOutput.toJson(value))
        }
    }
}

