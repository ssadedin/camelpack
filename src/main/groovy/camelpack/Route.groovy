package camelpack

import groovy.transform.ToString
import org.apache.camel.CamelContext
import org.apache.camel.impl.SimpleRegistry
import org.apache.log4j.Logger
import com.fasterxml.jackson.annotation.JsonIgnore

@ToString(includeNames=true, excludes=['definition','registry'])
class Route {
    
    String name
    
    File file
    
    String state
    
    String error
    
    long lastUpdated
    
    Logger logger
    
    @JsonIgnore
    SimpleRegistry registry = new SimpleRegistry()
    
    @JsonIgnore
    CamelContext context
    
    String getDefinition() {
        file.text
    }
}
