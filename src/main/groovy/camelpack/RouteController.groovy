package camelpack

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import groovy.util.logging.Log4j
import io.micronaut.http.HttpStatus

@Controller("/route")
@Log4j
class RouteController {
    
    ContextManager mgr
    
    RouteController(ContextManager mgr) {
        this.mgr = mgr
    }

    @Get("/")
    HttpStatus index() {
        return HttpStatus.OK
    }
    
    @Get("/list")
    List routes() {
        return mgr.getRoutes().grep { Route r -> r.file.exists() }
    }
    
    @Get("/{name}/toggle")
    Map toggle(String name) {
        Route route = mgr.getRoute(name)
        if(route.state == 'active') {
            log.info "Pause $name"
            mgr.pause(name)
        }
        else
        if(route.state == 'paused') {
            log.info "Unpause $name"
            mgr.unpause(name)
        }
        else {
            log.error "Route in invalid state for pause / unpause operation"
        }
        return [
            status: 'ok',
            route: route
        ]
    }
}