package camelpack

import groovy.text.SimpleTemplateEngine
import groovy.text.Template
import groovy.text.TemplateEngine
import groovy.util.logging.Log4j

class BoundTemplate extends Reader {
    
    Map variables
    
    Template template
    
    StringReader reader
    
    String toString() {
        template.make(variables).toString()
    }

    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        if(reader == null) {
            reader = new StringReader(this.toString())
        }
        return reader.read(cbuf, off, len)
    }

    @Override
    public void close() throws IOException {
        // noop
    }

} 

/**
 * Stores a cache of templates read from a local <code>routes/templates</code>
 * directory and exposes found templates as properties.
 * 
 * @author Simon Sadedin
 */
@Log4j
class Templates {

    File templateDir = new File("routes/templates")

    Map cache = null
    
    Map globals = [:]
    
    TemplateEngine templateEngine = new SimpleTemplateEngine()

    synchronized void initTemplates() {
        
        def templateFiles = templateDir.listFiles().grep { File f ->
            !f.directory
        }
        
        cache = templateDir.listFiles().grep { File f ->
            !f.directory
        }
        .collectEntries { File f ->
            [
                f.name.replaceAll('\\..*$',''),
                f
            ]
        }
        
        for(File f in templateFiles) {
            Templates.metaClass[f.name.replaceAll('\\..*$','')] << {
                return this.@templateEngine.createTemplate(f).make(it).toString()
            }
        }
    }
    
    def methodMissing(String name, args) {
        
        if(cache == null) {
            initTemplates()
        }
        
        if(!cache[name])
            throw new IllegalArgumentException("No template with name ${name}.* found in ${templateDir.absolutePath}")

        def template = this.@templateEngine.createTemplate(this.@cache[name])
        
        Map variables = args[0]
        if(!(variables instanceof Map)) {
            variables = variables.properties as Map
        }
        
        log.debug "Created new bound template for $name with variables $variables"

        if(!variables.containsKey('globals'))
            variables = [globals: globals] + variables

        return new BoundTemplate(template:template, variables: variables)
    }
    
   
    def getProperty(String name) {
        
        if(cache == null) {
            initTemplates()
        }
        
        if(!cache[name])
            throw new IllegalArgumentException("No template with name ${name}.* found in ${templateDir.absolutePath}")

        def template = this.@templateEngine.createTemplate(this.@cache[name])
        return { Map vars ->
            if(!vars.containsKey('globals')) {
                vars = [globals: vars] + globals
            }
            template.make(vars).toString()
        }
    }
}
