package camelpack.components

import java.util.regex.Pattern
import javax.inject.Inject
import org.apache.camel.Consumer
import org.apache.camel.Endpoint
import org.apache.camel.EndpointConfiguration
import org.apache.camel.Exchange
import org.apache.camel.ExchangePattern
import org.apache.camel.Processor
import org.apache.camel.Producer
import org.apache.camel.component.file.GenericFile
import org.apache.camel.impl.DefaultComponent
import org.apache.camel.impl.DefaultEndpoint
import org.apache.camel.impl.DefaultExchange
import org.apache.camel.impl.ScheduledPollConsumer
import org.apache.camel.spi.UriEndpoint; 
import org.apache.http.HttpResponse
import org.apache.http.client.methods.HttpPut
import org.apache.http.client.utils.URIBuilder
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.params.HttpParams
import org.gitlab4j.api.Constants.IssueState
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.IssuesApi
import org.gitlab4j.api.NotesApi
import org.gitlab4j.api.models.FileUpload
import org.gitlab4j.api.models.Issue
import org.gitlab4j.api.models.IssueFilter
import org.gitlab4j.api.models.IssueFilter.IssueField
import org.gitlab4j.api.models.Project
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import camelpack.BoundTemplate
import camelpack.ContextManager
import camelpack.Templates
import groovy.json.JsonSlurper
import groovy.text.SimpleTemplateEngine
import groovy.text.TemplateEngine
import groovy.transform.CompileStatic
import groovy.transform.MapConstructor

class GitlabProducer implements Producer {
    
    private static Logger logger = LoggerFactory.getLogger("GitlabProducer")

    GitlabEndpoint endpoint
    
    TemplateEngine templateEngine = new SimpleTemplateEngine()
    
    public GitlabProducer(GitlabEndpoint endpoint) {
        super();
        this.endpoint = endpoint;
        
        if(endpoint.issueId == null && endpoint.labels==null && endpoint.title == null && endpoint.titlePattern == null && endpoint.search == null) {
            throw new IllegalArgumentException("Cannot create producer for endpoint $endpoint One of issueId(s), labels, title, titlePattern or search must be specified")
        }
    }

    @Override
    public void process(Exchange exchange) throws Exception {

        logger.info "Procesing exchange $exchange"
        
        this.endpoint.initGitlabConnection()
        
        String content = generateContent(exchange)

         if(endpoint.simulateOnly) {
            logger.info "Would invoke gitlab call: $endpoint to post content:\n\n$content\n"
            return
        }

        List<Issue> issues = endpoint.queryIssues()
        
        logger.info "Found ${issues.size()} issues matching $endpoint"
        
        if(endpoint.maxIssues > 0 && issues.size() > endpoint.maxIssues) {
            logger.info "Taking first ${endpoint.maxIssues} of ${issues.size()} due to maxIssues=$endpoint.maxIssues"
            issues = issues.take(endpoint.maxIssues)
        }
        
       
        if(issues.isEmpty() && endpoint.minIssues>0) {
            if(endpoint.title == null && endpoint.newTitle) 
                throw new IllegalArgumentException(
                    "Title or newTitle must be specified for creation of new issues in Gitlab Producer" +
                    " (route ${exchange.fromRouteId} searched for: labels=$endpoint.labels, state=$endpoint.state, search=$endpoint.search, titlePattern=$endpoint.titlePattern)"
                    )

            logger.info "No issue found matching search criteria: will create issue"

            // Add global label filter labels to created tickets
            List<String> globalFilter = System.getProperty('camelpack.gitlab.global_label_filter', '').tokenize(',')
            if(globalFilter) {
                content += "\n" + globalFilter.collect { '/label ~"' + it + '"' }.join('\n')
            }
            
            String description = content
            if(this.endpoint.description) {
                logger.info "Separate description provided: message content as separate note"
                description = this.endpoint.description
            }

            def createdIssue = endpoint.gitlab.issuesApi.createIssue(endpoint.gitlabProject, endpoint.title?:endpoint.newTitle, description)
            if(this.endpoint.description) {
                endpoint.gitlab.notesApi.createIssueNote(endpoint.gitlabProject.id, createdIssue.iid, content)
            }
            issues = [createdIssue]
        }
        else {
            if(issues.size() < endpoint.minIssues)
                throw new IllegalStateException("Route ${exchange.fromRouteId}: minimum number of issues to post to ($endpoint.minIssues) not satisfied (issues found=${issues.size()}, searched for: labels=$endpoint.labels, state=$endpoint.state, search=$endpoint.search, titlePattern=$endpoint.titlePattern)")

            for(Issue issue in issues) {
                // If note is specified and blank, do not add any note
                if(endpoint.note != '')
                    endpoint.gitlab.notesApi.createIssueNote(endpoint.gitlabProject.id, issue.iid, content)
                else {
                    endpoint.adjustLabels(issue)
                }
            }
        }
        
        if(exchange.getPattern().equals(ExchangePattern.InOut))
            exchange.getOut().body = issues
    }

    /**
     * Inspect the exchange parameters and format body of outgoing message according to
     * the implied processing (eg: applying templates etc.)
     * 
     * @param exchange
     * @return
     */
    String generateContent(Exchange exchange) {
        def body = exchange.in.body
        def content
        if(endpoint.template) {
            logger.info "Replacing body with template $endpoint.template"

            def bodyAsMap = (exchange.in.body instanceof Map ? exchange.in.body : [body: exchange.in.body, exchange: exchange])
            
            body = ContextManager.templates.methodMissing(endpoint.template, [bodyAsMap])
        }

        if(body instanceof GenericFile) {
            logger.info "Body is file $body.absoluteFilePath : converting to file contents"
            content = body.file.text
        }
        else
        if(body instanceof BoundTemplate) {

            // If there are files in the template, upload them and replace references with
            // Gitlab upload paths
            body = uploadAndReplaceAttachmentReferences(body)

            content = body.toString()
        }
        else {
            content = body.toString()
        }
        return content
    }
    
    BoundTemplate uploadAndReplaceAttachmentReferences(BoundTemplate body) {
        
        GitlabEndpoint ep = endpoint
        
        Map gitlabVariables = body.variables.collectEntries { entry ->
            File file = null
            if(entry.value instanceof GenericFile) {
                file = ((GenericFile)entry.value).file
            }
            else
            if(entry.value instanceof File) {
                file = entry.value
            }
            
            if(file) {
                // Upload file
                File actualFile = file.absoluteFile
                logger.info "Uploading $actualFile to Gitlab project $ep.gitlabProject.id"
                FileUpload result = ep.gitlab.projectApi.uploadFile(ep.gitlabProject.id, actualFile)
                return [ entry.key, "[$file.name]($result.url)"]
            }
            else {
                return [ entry.key, entry.value ]
            }
        }
        
        return new BoundTemplate(template: body.template, variables:gitlabVariables)
    }

    @Override
    public void start() throws Exception {
        // noop
    }

    @Override
    public void stop() throws Exception {
        //noop
    }

    @Override
    public boolean isSingleton() {
        return false;
    }

    @Override
    public Endpoint getEndpoint() {
        return endpoint
    }

    @Override
    public Exchange createExchange() {
        return new DefaultExchange(this.endpoint)
    }

    @Override
    public Exchange createExchange(ExchangePattern pattern) {
        Exchange e = new DefaultExchange(this.endpoint, pattern)
        return e;
    }

    @Override
    public Exchange createExchange(Exchange exchange) {
        return new DefaultExchange(exchange)
    }
}

class GitlabConsumer extends ScheduledPollConsumer implements Runnable {

    GitlabEndpoint endpoint
    
    private static Logger logger = LoggerFactory.getLogger("GitlabConsumer")
    
    public GitlabConsumer(Endpoint endpoint, Processor processor) {
        super(endpoint, processor);
        this.endpoint = endpoint
        
        if(this.endpoint.interval > 0) {
            this.delay = this.endpoint.interval * 1000
        }
        else
        if(super.getDelay() < 5000)
            this.delay = 120000
    }

    @Override
    protected int poll() throws Exception {
       Date now = new Date();

       List<Issue> issues = endpoint.queryIssues()
       
       int processed = 0
       
       List multipleBody = []
       for(Issue issue in issues) {

          
           if(endpoint.multiple)
               multipleBody.add(issue)
           else {
               Exchange exchange = endpoint.createExchange();
               exchange.getIn().setBody(issue)
               exchange.getIn().setHeader('CAMELPACK_GITLAB_PROJECT_ID', String.valueOf(issue.projectId))
               exchange.getIn().setHeader('CAMELPACK_GITLAB_ISSUE_ID', String.valueOf(issue.iid))
               exchange.setProperty('CAMELPACK_GITLAB_ISSUE_ID', issue.iid)
               exchange.setProperty('CAMELPACK_GITLAB_PROJECT_ID', issue.projectId)
               getProcessor().process(exchange);
           }
              

           ++processed
           
           if(endpoint.add || endpoint.remove) {
               
               // If there is a note, we add / remove the labels using gitlab actions within the note so that it 
               // is a single API call. Otherwise, we do it directly here with a PUT to the issue
               if(endpoint.note == '') {
                   endpoint.adjustLabels(issue)
               }
               else {
                   String labelAdds = endpoint.add?.tokenize(',')?.collect {
                       '/label ~"'+it+'"'
                   }?.join('\n')?:""
                   
                   String labelRemoves = endpoint.remove?.tokenize(',')?.collect {
                       '/unlabel ~"'+it+'"'
                   }?.join('\n')?:""
                   
                   String note = endpoint.note

                   // Make sure that there is newline before we add /label
                   if(note != null && !note.endsWith('\n') && (labelAdds || labelRemoves))
                       note = note + '\n'
                  
                   log.info("Create issue note on issue $issue.id")
                   endpoint.gitlab.notesApi.createIssueNote(
                       endpoint.gitlabProject.id, 
                       issue.iid, 
                       (note ?: "Processed automatically on $now\n") + "$labelAdds\n$labelRemoves\n"
                   )
               }
           }
           ++processed
       }
       
       if(multipleBody) {
           Exchange exchange = endpoint.createExchange();
           exchange.getIn().setHeader('CAMELPACK_GITLAB_PROJECT_ID', String.valueOf(multipleBody[0].projectId))
           exchange.getIn().setHeader('CAMELPACK_GITLAB_ISSUE_ID', String.valueOf(multipleBody*.iid).join(','))
           exchange.getIn().setBody(multipleBody)
           getProcessor().process(exchange);
       }
       
       return processed
    }

}

/**
 * An endpoint that polls a gitlab project and forwards issues that match a specified
 * title pattern and having / excluding specific labels. To prevent issues being 
 * returned repeatedly, labels can be specified for removal upon processing.
 * 
 * @author Simon Sadedin
 */
@MapConstructor
@UriEndpoint(scheme="gitlab", syntax="gitlab:name", title="Gitlab", consumerClass=GitlabConsumer)
class GitlabEndpoint extends DefaultEndpoint {
    
    private static Logger logger = LoggerFactory.getLogger("GitlabEndpoint")
    
    /**
     * Gitlab token to use for this specific component / end point. Defaults to value set by env variable or system property
     */
    String serverURL
    
    /**
     * Environment variable to use to access token. Defaults to GITLAB_TOKEN.
     */
    String tokenEnv = "GITLAB_TOKEN"

    /**
     * Comma separated labels to search for. By default, issues will only be returned if the
     * label is present. However a label prefixed by - will cause issues to be excluded if the 
     * label is found.
     */
    List<String> labels
    
    
    String search
    
    String titlePattern
    
    /**
     * Used only for producer: title of issue to create
     */
    String title
    
    /**
     * Used for producer to specify title of issue to create when 
     * issues are matched using a pattern instead of specific title
     */
    String newTitle
    
    /**
     * Specific issue id to query
     */
    String issueId

    /**
     * Project to poll issues from. By default, this is the name of the host in the endpoint, ie:
     * 
     * gitlab:&lt;project&gt;?params ....
     */
    String project
    
    /**
     * Labels to remove when message is processed
     */
    String remove

    /**
     * Labels to add when message is processed
     */
    String add
    
    /**
     * Note to add when issue is processed
     */
    String note
    
    /**
     * Description to use : causes message content or template to be applied as a note, while 
     * the description becomes the description of the issue. If unset, message content
     * will by default be used for the description if issue does not exist, or as note if it does.
     */
    String description
    
    /**
     * Template to apply (Producer only)
     */
    String template
    
    /**
     * State of issues to consider - OPENED,CLOSED,ALL
     */
    String state
    
    /**
     * Only return issues that were created less than maxAgeMs ago
     */
    Long maxAgeSeconds = null
   
    /**
     * Whether to process single issues at a time or multiple
     */
    boolean multiple
    
    /**
     * Maximum number of issues to process when operating on multiple issues
     */
    int maxIssues = 1
    
    /**
     * Minimum number of issues to process when producing output to Gitlab
     */
    int minIssues = 1
    
    /**
     * Causes the component to validate all parameters but not actually interact with a real gitlab instance
     */
    boolean simulateOnly = Boolean.parseBoolean(System.getProperty('camelpack.gitlab.enable_simulation', 'false'))
    
    long interval = -1
    
    GitLabApi gitlab 
    
    Project gitlabProject
    
    Pattern titlePatternRegex
    
    GitlabEndpoint(String uri, GitlabComponent component) {
        super(uri, component);
    }
    
    void setLabels(String labelString) {
        this.labels = labelString.tokenize(',')*.trim()
    }
    
    void setMultiple(String multipleString) {
        this.multiple = Boolean.parseBoolean(multipleString)
    }

    @Override
    public Consumer createConsumer(Processor processor) throws Exception {
        
        logger.info "Create consumer for " + super.endpointUri + "(key = " + super.endpointKey + ") with path " + super.getEndpointConfiguration().getParameter(EndpointConfiguration.URI_PATH)
        
       
        if(template != null) 
            throw new IllegalArgumentException("Template argument is not supported for Gitlab Consumer")
        
        return new GitlabConsumer(this, processor)
    }
    
    @CompileStatic
    void initGitlabConnection() {

        if(gitlab)
            return // already initialised 

        String gitlabServer = this.serverURL?:System.getenv("GITLAB_SERVER")?:System.getProperty('camel.gitlabServer')
        
        if(!gitlabServer) 
            throw new IllegalStateException("Unable to create Gitlab Component because neither GITLAB_SERVER environment variable or camel.gitlabServer property are set")
            
        String gitlabToken = System.getenv(tokenEnv)?:System.getProperty('camel.gitlabToken')
        
        if(!gitlabToken) 
            throw new IllegalStateException("Unable to create Gitlab Component because neither GITLAB_TOKEN environment variable or camel.gitlabToken property are set")
    
        gitlab = new GitLabApi(gitlabServer, gitlabToken)
    
        if(titlePattern) {
            titlePatternRegex = ~titlePattern
        }
        
        String project = this.project ?: new URI(this.endpointUri).host ?: new URI(this.endpointUri).authority 
        
        logger.info "Connecting end point $endpointUri to Gitlab project $project"
        
        try {
            if(project.isInteger()) {
                this.gitlabProject = gitlab.projectApi.getProject(project.toInteger())
            }
            else {
                this.gitlabProject = gitlab.projectApi.getProjects(project).find { it.name == project}
            }
            
            logger.info "Connected to project $gitlabProject.id ($gitlabProject.name)"
        }
        catch(Exception e) {
            logger.error("Unable to resolve project based on specifier: $project ($e)", e)
        }
    }

    @Override
    public Producer createProducer() throws Exception {

        return new GitlabProducer(this)
    }

    @Override
    public boolean isSingleton() {
        return false;
    }
    
    /**
     * Query for issues matching the labels, title and search parameters of the endpoint
     * 
     * @return List of issues found
     */
    @CompileStatic
    List<Issue> queryIssues() {
       
        this.initGitlabConnection()
        
        Map<String,List<String>> labels = this.labels
            ?.groupBy { it.startsWith('-') ? 'exclude' : 'require' }
            ?.collectEntries { e -> [ e.key, e.value.collect { String label -> label.startsWith('-') ? label.substring(1) : label }]}
            ?:[:]

        List<String> globalFilter = System.getProperty('camelpack.gitlab.global_label_filter', '').tokenize(',')
        List<String> requiredLabels = ((List<String>)labels.require) ?: []
        IssueFilter filter = new IssueFilter(
                labels : (requiredLabels + globalFilter).unique(),
                search: this.search,
        )
        
        if(this.maxAgeSeconds != null) {
            filter.createdAfter = new Date(System.currentTimeMillis()-(this.maxAgeSeconds*1000))
        }
        
        if(labels.exclude) {
            Map<IssueField, Object> labelFilter = (Map<IssueField, Object>)[ (IssueField.LABELS) : (String)labels.exclude.join(',')]
            filter = filter.withNot(labelFilter)
        }
        
        if(this.issueId) {
            filter.iids = this.issueId.tokenize(' ,')
        }

        if(this.state) {
            if(this.state.toUpperCase() == 'ALL') {
                // noop: do not add state filter and all issues are returned
            }
            else
                filter.state = IssueState.forValue(this.state.toUpperCase())
        }
        else {
            filter.state = IssueState.OPENED
        }
        
        List<Issue> allIssues = this.gitlab.issuesApi.getIssues(this.gitlabProject, filter)
        
       
        List<Issue> issues = allIssues
        
        if(this.title) {
            issues = issues.grep { Issue issue -> issue.title == this.title }
        }
        else
        if(this.titlePattern) {
            issues = issues.grep { Issue issue -> 
                issue.title ==~ this.titlePatternRegex 
            }
        }

        if(search)
            logger.info "Found ${issues.size()} issues matching $search with labels $requiredLabels, excluding $labels.exclude"
        else
            logger.info "Found ${issues.size()} issues with labels $requiredLabels, excluding $labels.exclude"

        return issues
    }
    
    final static List<String> uriParams = [
                        'serverURL',
                        'tokenEnv',
                        'search',
                        'titlePattern',
                        'title',
                        'issueId',
                        'remove',
                        'add',
                        'note',
                        'template',
                        'state',
                        'labels',
                        'multiple',
                        'interval',
                        'minIssues',
                        'maxIssues',
                        'newTitle',
                        'simulateOnly',
                        'maxAgeSeconds'
                    ].unique()    
        
    @CompileStatic
    String uri(Map additionalParams) {
        
        if(additionalParams == null)
            additionalParams = [:]
        
        if(issueId)
            return ["gitlab:$project?issueId=$issueId", *additionalParams.collect { Map.Entry e -> "$e.key=$e.value" }].join('&')
        
        String paramString = uriParams.findAll {
            // is set and is not overridden by user param
            (this[it] != null) && !additionalParams.containsKey(it)
        }
        .collect {
            if(this[it] instanceof List)  
                "$it=${this[it].join(',')}"
            else
                "$it=${this[it]}"
        }
        .join('&')

        return ["gitlab:$project?$paramString", *additionalParams.collect { Map.Entry e -> "$e.key=$e.value" }].join('&')
    }
    
    /**
     * Adjust the labels on an issue without adding a note or body
     * 
     * @param issue
     */
    void adjustLabels(Issue issue) {
        URI uri = new URIBuilder(gitlab.gitLabServerUrl + "/api/v4/projects/$gitlabProject.id/issues/$issue.iid")
                .setParameter('add_labels', add)
                .setParameter('remove_labels', remove)
                .build()

        HttpPut put = new HttpPut(uri)
        put.setHeader('PRIVATE-TOKEN', gitlab.authToken)

        logger.info "Updating labels via direct issue PUT to $uri"

        CloseableHttpClient client = HttpClients.createDefault()
        client.withCloseable {
            HttpResponse response = client.execute(put)
            if(response.statusLine.statusCode != 200) {
                throw new IOException('Unable to set labels using uri ' + uri + ' with status ' + response.statusLine)
            }
        }
    }
}

class GitlabComponent extends DefaultComponent {
    
    private static Logger logger = LoggerFactory.getLogger("GitlabComponent")
    
    protected Endpoint createEndpoint(String uri, String remaining, Map<String, Object> parameters) throws Exception { 
        
        Endpoint endpoint = new GitlabEndpoint(uri, this);
        
        setProperties(endpoint, parameters);
        return endpoint; 
    }
}


