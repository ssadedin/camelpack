package camelpack

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.micronaut.core.annotation.*

@Controller("/")
class StaticController {

    @Produces(MediaType.TEXT_HTML)
    @Get("/camel{/path:.*}")
    String index(@Nullable String path) {
        return new File('build/js/index.html').text
    }
  
    @Produces('text/css')
    @Get("/css{/path:.*}")
    String css(@Nullable String path) {
        return new File("build/js/css/$path").text
    } 
     
    @Produces('text/javascript')
    @Get("/js{/path:.*}")
    String js(@Nullable String path) {
        return new File("build/js/js/$path").text
    }  
}
