package camelpack

import org.apache.camel.Exchange
import org.apache.camel.NoTypeConversionAvailableException
import org.apache.camel.TypeConversionException
import org.apache.camel.TypeConverter
import org.springframework.core.style.ToStringCreator
import groovy.json.JsonBuilder
import groovy.json.JsonGenerator

/**
 * A utility class to support "model classes" that easily accept conversion
 * to and from JSON objects, providing for typesafe operations on known properties
 * but allowing for additional properties in the data.
 * 
 * @author simon.sadedin
 */
class Model extends Reader {
    
    protected List<String> quietProperties = []
    
    protected Map<String,Object> undeclared = [:]
    
    protected JsonGenerator.Options jsonOptions = new JsonGenerator.Options()
    
    protected StringReader thisReader = null

    /**
     * If attempting to set an unknown property, create the property on the fly.
     * 
     * This behaviour allows for incoming objects with unknown properties to 
     * not fail due to the unknown property.
     * 
     * @param name
     * @param value
     */
    void propertyMissing(String name, Object value) {
        this.undeclared[name] = value
    }
    
    /**
     * If attempting to read an unknown property, throw an exception, unless
     * the property was bound explicitly.
     */
    def propertyMissing(String name) {
       if(undeclared.containsKey(name))
            return undeclared[name]
        else
            throw new IllegalArgumentException("Property $name is not available on type " + this.class.name)
    }
    
    String toString() {
        "${this.class.name}(" + this.exportedProperties.grep { !(it.key in quietProperties) }.collectEntries() + ")"
    }
    

    public int read(char[] cbuf, int off, int len) throws IOException {
        if(thisReader == null)
            thisReader = new StringReader(this.toJSON())
        return thisReader.read(cbuf, off, len)
    }
    
    String toJSON() {
        def generator = jsonOptions.build()
    
        Map props = getExportedProperties()
        
        return new JsonBuilder(props, generator).toPrettyString()
    }

    private Map getExportedProperties() {
        Map props = new LinkedHashMap(this.properties)
        props.remove('class')
        props.remove('undeclared')
        props.remove('thisReader')
        props.remove('quietProperties')
        props.remove('jsonOptions')

        props += undeclared
        return props
    }

    public void close() throws IOException {
    }
}
