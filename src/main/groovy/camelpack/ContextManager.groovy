/**
 *
 * Camel Pack 
 *
 * This service monitors the route folder and allows routes to be added / modified / enabled / disabled dynamically
 *
 */
package camelpack

import static org.apache.activemq.camel.component.ActiveMQComponent.activeMQComponent;

import java.nio.file.FileSystems
import java.nio.file.Path
import java.nio.file.StandardWatchEventKinds
import java.nio.file.WatchEvent
import java.nio.file.WatchKey
import java.nio.file.WatchService

import org.apache.activemq.camel.component.ActiveMQComponent
import org.apache.camel.CamelContext
import org.apache.camel.Component
import org.apache.camel.Exchange
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.file.GenericFile
import org.apache.camel.impl.DefaultCamelContext
import org.apache.camel.impl.SimpleRegistry
import org.apache.camel.model.FromDefinition
import org.apache.camel.model.OnExceptionDefinition
import org.apache.camel.model.ProcessorDefinition
import org.apache.camel.model.ToDefinition
import org.apache.camel.processor.DefaultExchangeFormatter
import org.apache.camel.spi.CamelContextNameStrategy
import org.apache.camel.util.MessageHelper

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import groovy.text.SimpleTemplateEngine
import groovy.text.Template
import groovy.text.TemplateEngine
//import ch.grengine.*
import groovy.transform.CompileStatic
import groovy.transform.Memoized
import groovy.util.logging.Log4j
import groovy.util.logging.Slf4j
import io.micronaut.scheduling.annotation.Scheduled

import org.apache.log4j.BasicConfigurator
import org.apache.log4j.ConsoleAppender
import org.apache.log4j.FileAppender
import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.log4j.PatternLayout
import org.codehaus.groovy.control.CompilerConfiguration
import org.codehaus.groovy.runtime.StackTraceUtils
import org.slf4j.LoggerFactory

import javax.inject.Singleton

@CompileStatic
class RouteDefinition {
    
    RouteDefinition(File routeFile) {
        List<String> nonImportLines = []
        List<String> importLines = []
        
        routeFile.eachLine { String line ->
            if(line.startsWith('import'))
                importLines << line
            else
                nonImportLines << line
        }
        
        this.imports = importLines.join('\n')
        this.routes = nonImportLines.join('\n')
    }
    
    SimpleRegistry registry
    String imports 
    String routes
    
}

@Singleton
@Slf4j
class ContextManager {
    
    @Delegate
    JSONHelpers jsonHelpers = new JSONHelpers()
    
    File routesDir = new File("routes")
    
    File libsDir = new File("routes/lib")
    
    Map<String,Long> routeTimestamps = [:]
    
    List<Route> routes = []
    
    static Map globals = Collections.synchronizedMap([:])
    
    static Templates templates = new Templates(globals:globals)

    GroovyClassLoader routeClassLoader
    
    WatchService watcher
    
    static {
        BasicConfigurator.configure()
        Logger.getRootLogger().setLevel(Level.INFO)
    }
    
    ContextManager() {
        log.info "Created context manager in directory: " + new File(System.properties['user.dir'])

       
        this.createClassLoader()
       
        watcher = FileSystems.getDefault().newWatchService()
        this.routesDir.toPath().register(watcher,[StandardWatchEventKinds.ENTRY_MODIFY] as WatchEvent.Kind[], 
            com.sun.nio.file.SensitivityWatchEventModifier.HIGH)
        
        new File(routesDir, 'models').toPath().register(watcher,[StandardWatchEventKinds.ENTRY_MODIFY] as WatchEvent.Kind[], 
            com.sun.nio.file.SensitivityWatchEventModifier.HIGH)
        
        ConfigObject config = this.getConfig()
        if(config.getOrDefault('suppressFullStacktraces', false))
            this.showSimplifiedStacktraces()

        new Thread(this.&watchRoutes).start()
    }
    
    @CompileStatic
    void watchRoutes() {
        while(true) {
            WatchKey k = watcher.take()
            boolean update = false
            for(WatchEvent e : (List<WatchEvent>)k.pollEvents()) {
                Path path = (Path)e.context()
                if(path.fileName.toString().endsWith('.groovy')) {
                    if(new File('models', path.fileName.toString()).exists()) {
                        log.info "Updating all routes to modified model file $path"
                        this.routes.each { it.lastUpdated = 0L  }
                    }
                    else {
                        log.info "Updating due to modified groovy file $path"
                    }
                    update = true
                }
            }

            if(update) {
                // Force to re-read config
                forceUpdateConfig = true
                this.getConfig()
                setup()
            }
            
            k.reset()
        }
    }
    
    @CompileStatic
    void createClassLoader() {
        Collection<File> jars = libsDir.listFiles().findAll { File libFile -> libFile.name.endsWith('.jar') }
        if(!jars.isEmpty()) {
                log.info "Found jars to add to classpath: " + jars.join(',')
        }
         this.routeClassLoader = new GroovyClassLoader(this.class.classLoader)
        jars.each { 
            this.routeClassLoader.addClasspath(it.absolutePath)
        }

        File modelsDir = new File(routesDir,'models')
        if(modelsDir.exists()) {
            routeClassLoader.addClasspath(modelsDir.absolutePath)
        }

        File classesDir = new File(libsDir,'classes')
        if(classesDir.exists()) {
            routeClassLoader.addClasspath(classesDir.absolutePath)
        }
        
        this.templates.templateEngine = new SimpleTemplateEngine(this.routeClassLoader)
    }
    
    void setup() {
        
        getRouteFiles().each { File routeFile ->
            updateRoute(routeFile)
        }
        pauseMissingRoutes()
    }

    private void pauseMissingRoutes() {
        for(Route route in routes) {
            if(!route.file.exists() && route.state != "paused") {
                log.info "File for route $route.name no longer exists: pausing route"
                pause(route.name)
                return
            }
        }
    }
    
    /**
     * @return true if the specified route name is disabled in the configuration
     */
    @CompileStatic
    boolean isDisabled(String routeName) {
        List enabledRoutes = (List)config.getOrDefault('enabled', null)
        if(enabledRoutes && !(routeName in enabledRoutes))
            return true
            
        List disabledRoutes = (List)config.getOrDefault('disabled', null)
        if(disabledRoutes && (routeName in disabledRoutes))
            return true
            
        return false
    }
    
    /**
     * A list of routes to force update for other reasons than that their timestamp changed
     */
    List<String> forceUpdate = []
    
    void updateRoute(File routeFile) {
        Route route
        try {
            
            List enabledRoutes = config.getOrDefault('enabled', null)
            route = getRoute(routeFile)
            if(isDisabled(route.name)) {
                if(route.state == 'active') {
                    this.pause(route.name)
                    route.state = 'disabled'
                }
                return
            }
            
            if(forceUpdate.contains(route.name)) {
                log.info("Force updating $route.name : most likely due to config update")
            }
            
            long routeTimestampMs = route.lastUpdated
            if(forceUpdate.contains(route.name) || (routeTimestampMs < routeFile.lastModified())) {
                log.info "Refreshing route $routeFile (modified)"
                
                // ensure config gets re-read if its route is updated,
                // otherwise there is a 3 second delay
                this.configTimestampMs = 0L
                setup(route)
            }
        }
        catch(Exception e) {
            StackTraceUtils.sanitize(e)
            log.error("Failed to set up routes from ${route?.name}: $e", e)
            if(route != null)
                route.state = 'error'
        }
        finally {
            forceUpdate.removeIf { routeName -> routeName == route?.name  }
        }
        
    }
    
    List<File> getRouteFiles() {
       routesDir.listFiles().grep { it.name.endsWith('.groovy') && !it.name.equals('config.groovy') && !it.name.equals('local_config.groovy') }
    }
    
    long configTimestampMs = 0L
    
    private boolean forceUpdateConfig = false
    private ConfigObject privateConfig = null
    
    File configFile = new File(routesDir,'config.groovy')

    File localConfigFile = new File(routesDir,'local_config.groovy')

    @CompileStatic
    synchronized ConfigObject getConfig() {
        
        ConfigObject config = privateConfig
        
        // Check if config file was updated
        if(!forceUpdateConfig && config != null) {
            
            // Check at most once every 3 seconds
            long nowMs = System.currentTimeMillis()
            if(nowMs < configTimestampMs + 3000) {
                return config
            }
            
            // note: lastModified returns 0 if the file does not exist
            long configMs = Math.max(configFile.lastModified(), localConfigFile.lastModified())
            if(configMs <= configTimestampMs)
                return config
        }
        
        this.forceUpdateConfig = false
        
        String configEnv = System.properties.getOrDefault('camelpack.env', null) ?: System.getenv('CAMELPACK_CONFIG_ENV')
        ConfigSlurper configReader = configEnv ? new ConfigSlurper(configEnv) : new ConfigSlurper()
        
        config = new ConfigObject()
        if(configFile.exists()) {
            log.info "Reading config from $configFile"
            configTimestampMs = Math.max(configTimestampMs, configFile.lastModified())
            config = configReader.parse(configFile.text)
        }
            
        if(localConfigFile.exists()) {
            log.info "Reading local config from $localConfigFile"
            configTimestampMs = Math.max(configTimestampMs, localConfigFile.lastModified())
            config.merge(configReader.parse(localConfigFile.text))
        }
        
        // Before updating, find which routes now have different settings
        Collection<Map.Entry<String, ConfigObject>> routesWithModifiedConfig = config?.routes?.findAll { Map.Entry<String, ConfigObject> cfgEntry ->
            ConfigObject oldConfig = (ConfigObject)((ConfigObject)(this.privateConfig?.routes))?.getOrDefault(cfgEntry.key, null)
            
            Map oldMap = oldConfig.grep { Map.Entry e -> e.key != 'env' }.collectEntries()
            return oldMap != cfgEntry.value
        }

        if(routesWithModifiedConfig) {
            log.info("The following routes had modified configuration files and will be force updated: " + routesWithModifiedConfig*.key.join(", "))
            this.forceUpdate.addAll(routesWithModifiedConfig*.key)
        }
        
        this.privateConfig = config
        
        return privateConfig
    }
    
    /**
     * Convenience method that allows fetching values from system environment
     * in a type safe way, and also allowing for override within local config file.
     * 
     * <li>if the value is set in local config file then this takes precedence
     * <li>otherwise, the value is attempted to be retrieved from system environment
     * <li>if neither is available, the provided default is used
     * 
     * Note: the method here is not typesafe, but the IDE support class {@link RouteBuilderScript}
     * provides a generics enabled type safe version for IDE use.
     */
    Object getEnv(String routeName, String key, defaultValue, Class type=null) {
        
        ConfigObject config = this.config.routes[routeName]

        if(config.containsKey(key))
            return type ? config[key].asType(type) : config[key]

        if(type == null && defaultValue != null)
            type = defaultValue.class

        def envValue = System.getenv(key)
        if(envValue != null && envValue != '') {
            if(!type)
                return envValue
            
            if(type == Boolean) {
                return Boolean.parseBoolean(envValue)
            }
            else
                return envValue.asType(type)
        }
        
        if(type) {
            if(type == Boolean) {
                return Boolean.parseBoolean(String.valueOf(defaultValue))
            }
            else
                return defaultValue.asType(type)
        }
        else {
            return defaultValue
        }
    }
    
    List<Route> getRoutes() {
        return routes
    }
    
    Route getRoute(File routeFile) {
        String routeName = routeFile.name.replaceAll('.groovy$','')
        Route route = getRoute(routeName)
        if(route == null) {

            route = new Route(
                name: routeName,
                lastUpdated: 0L,
                state: 'New',
                file: routeFile
            )
            
            if(isDisabled(routeName)) {
                log.info "New route $routeName (disabled)"
            }
            else
                log.info "New route $routeName"
            
            this.routes << route
        }
        return route
    }
    
    Route getRoute(String name) {
        routes.find { it.name == name }
    }
  
    void stop() {
        routes*.context*.stop()
    }
    
   
    synchronized void setup(Route route) {
        
        CamelContext context = route.context
        if(context)
            context.stop()
            
        if(route.state == 'paused') {
            log.info "Skip setup of $route because it is paused"
            return
        }
        log.info "Setup $route"
            
        context = route.context = new DefaultCamelContext(route.registry)
        context.setNameStrategy(new CamelContextNameStrategy() {
            @Override
            public String getName() {
                return route.name;
            }

            @Override
            public String getNextName() {
                return getName();
            }

            @Override
            public boolean isFixedName() {
                return true;
            }
        })
        def activemq = configureActiveMQ()
        
        context.addComponent("activemq", activemq);
        applyComponentConfiguration(context)
        context.addRoutes(createRouteBuilder(route))
        
        setDefaultIds(context)

        context.start()
        route.lastUpdated = route.file.lastModified()
        route.state = 'active'
        log.info "Done updating $route.file"
    }

    /**
     * By default, the ids that get set onto route entries are non-descriptive, so unless
     * you assign all of them manually they are not very helpful when seen in traces, logs or
     * in hawtio. This method renames them based on their type and destination where those
     * make sense.
     * 
     * @param context   context to rename route ids for
     */
    private void setDefaultIds(CamelContext context) {
        Map<String, List> toDefs = [:]

        context.getRouteDefinitions().each { routeDef ->
            try {
                List<ProcessorDefinition> outputs = routeDef.getOutputs().grep { it.class != OnExceptionDefinition }
                ProcessorDefinition proc = outputs[1]
                FromDefinition from = routeDef.inputs[0]
                if(routeDef.id == null || routeDef.id ==~ /route[0-9]{1,}/) {
                    String newName = routeDef.id
                    if(from.id)
                        newName = 'route_' + from.id
                    else {
                        URI uri = new URI(from.uri)
                        if(uri.host) {
                            newName = uri.scheme + ':' + uri.host
                        }
                        else {
                            List parts = from.uri.tokenize(':?')
                            // For rest: components, the 3rd component contains the URL which is very nice to see
                            if(from.uri.startsWith('rest:'))
                                newName = parts[0] + ':' + parts[1] + ':' + parts[2]
                            else
                                newName = parts[0] + ':' + parts[1]
                        }
                    }
                    
                    if(newName != null) {
                        log.info "Renaming route $routeDef.id to $from.id"
                        routeDef.id = newName
                    }
                }

                routeDef.outputs.grep { it.class == ToDefinition }.each { ToDefinition  to ->
                    if(to.id == null)  {
                        String baseId = to.id = 'to_' + to.getUriOrRef().tokenize(':?/')[0] + '_' + to.getUriOrRef().tokenize(':?/')[1]
                        int index = toDefs.get(baseId, []).size()
                        to.id = baseId + '_' + index
                        toDefs.get(baseId).add(to.id)
                    }
                }
            }
            catch(Exception e) {
                log.info "Unable to automatically assign id for $routeDef : $e"
            }
        }
    }

    /**
     * @return a configured activemq component based on system properties / environment variables
     */
    private ActiveMQComponent configureActiveMQ() {
        String activemqHost = System.getProperty('activemq.host', 'activemq')
        String activemqPort = System.getProperty('activemq.port','61616')
        def activemq = activeMQComponent("tcp://$activemqHost:$activemqPort")

        String activemqUsername = System.getProperty('activemq.username')
        if(!activemqUsername)
            activemqUsername = System.getenv('CAMELPACK_ACTIVEMQ_USERNAME')

        if(activemqUsername) {
            // Allow password from environment or from system property
            String activemqPassword = System.getProperty('activemq.password')
            if(!activemqPassword) {
                activemqPassword = System.getenv('CAMELPACK_ACTIVEMQ_PASSWORD')
                if(!activemqPassword)
                    throw new IllegalArgumentException("Activemq username was set but no password was provided. Plese set the password in the activemq.password property or CAMELPACK_ACTIVEMQ_PASSWORD environment variable")
            }
            activemq.username = activemqUsername
            activemq.password = activemqPassword
        }
        return activemq
    }
    
    RouteBuilder createRouteBuilder(Route route) {
        
        RouteDefinition routeDef = new RouteDefinition(route.file)

        Logger routeLog = configureRouteLogging(route)
        
        return new RouteBuilder() {
            void configure() {
              log.info "Evaluate $route.name ($route.file)"
              
              ConfigObject routeConfig = config.routes[route.name]

              routeConfig.env =  { key, defaultValue, Class c = null ->
                  this.getEnv(route.name, key, defaultValue, c)
              }
              
              // Add globally accessible properties to registry so that
              // they can be looked up in explicitly as well as implicitly through
              // binding
              route.registry.put("globals", globals)
              route.registry.put("config", routeConfig)
              route.registry.put("log", routeLog)
  
              def binding = new Binding(
                  route: route,
                  globals:this.globals, 
                  registry: route.registry, 
                  log: routeLog, 
                  templates: templates,
                  parseJSON: this.&parseJSON,
                  toJSON: this.&toJSON,
                  routeBuilder:this,
                  config: config.routes[route.name],
                  stacktrace: this.&stacktrace,
                  diagnostics: { this.diagnostics(route, it) },
                  rootCause: this.&rootCause,
                  require: { require(route.name, it) }
              )

              def config = new CompilerConfiguration()
              config.scriptBaseClass = 'camelpack.RouteBuilderScriptBase'
              
              onException(Throwable).process { e ->
                  Exception ex = e.exception?:e.getProperty(Exchange.EXCEPTION_CAUGHT, Exception)
                  if(ex instanceof java.lang.reflect.UndeclaredThrowableException && ex.getCause())
                      ex = ex.getCause()

                  StackTraceUtils.deepSanitize(ex)
                  DefaultExchangeFormatter formatter = new DefaultExchangeFormatter();
                  formatter.setShowExchangeId(true);
                  formatter.setMultiline(true);
                  formatter.setShowHeaders(true);
                  formatter.setStyle(DefaultExchangeFormatter.OutputStyle.Fixed);
                  
                  routeLog.error(MessageHelper.dumpMessageHistoryStacktrace(e, formatter, false))

                  routeLog.error "Uncaught / handled error", ex
              }

              GroovyShell shell = new GroovyShell(routeClassLoader, binding, config)
             
              RouteBuilderScriptBase script = (RouteBuilderScriptBase)shell.parse(route.file)
              script.binding = binding
              script.setRouteBuilder(this)
              script.run()
            }
        }        
    }
    
    /**
     * Returns a useful diagnostic dump for a message. Exposed to routes as "diagnostics" method.
     */
    private String diagnostics(Route route, Exchange e) {
        
        String stack = stacktrace(e,4)

        int columnWidth = 45
        String propTable = 
            " | " + "Property".padRight(columnWidth) + " | " + "Value".padRight(columnWidth) + " |\n"  +
            " |" + "-" * ((columnWidth * 2) + 5) + "|\n" +
            e.properties
                .grep { !(it.key in ["CamelMessageHistory", "CamelBinding"]) }
                .collect { " | " + String.valueOf(it.key).padRight(columnWidth) + " | " + String.valueOf(it.value).padRight(columnWidth) + " |" }.join('\n')
            
        String result = "Properties:\n\n" + propTable;
        
        if(stack)
            result += "\n\nStack trace:\n\n" + stack
    }
    
    @CompileStatic
    private String stacktrace(Exchange e, int indentSpaces=0) {
        Exception ex = e.exception?:e.getProperty(Exchange.EXCEPTION_CAUGHT, Exception)
        Collection<StackTraceElement> stack = null 
        if(ex) {
            stack = StackTraceUtils.sanitizeRootCause(ex).getStackTrace().grep { 
                StackTraceElement el ->
                !el.className.startsWith('org.apache.camel.processor') && 
                !el.className.contains('jdk.internal.reflect') && 
                !el.className.startsWith('org.springframework.jms.listener.')
            }
        }
        
        String indent = " " * indentSpaces
        return indent + ex?.toString() + "\n" + stack.collect { indent + it }.join('\n') + '\n'
    }
    
    /**
     * Utility to get simplified root cause of exception that removes superfluous Groovy 
     * intermediate exceptions
     * @param e
     * @return
     */
    private Exception rootCause(Exchange e) {
        Exception ex = e.exception?:e.getProperty(Exchange.EXCEPTION_CAUGHT, Exception)        
        Exception rootCause = ex
        
        // Dig through at most 5 causes - hard limit to avoid any chance of circularity
        // resulting in infinite loop
        int depth = 0
        while(ex.cause != null && ex.cause != ex && (depth++ < 5)) { ex = ex.cause }
        return ex
    }
    
    /**
     * Start a dependent route : wired to <code>requires</code> function exposed to routes
     * 
     * @param routeName Name of route file to start
     */
    private void require(String requirer, String routeName) {
        File routeFile = new File(routesDir, routeName)
        if(!routeFile.exists()) {
            routeFile = new File(routesDir, routeName + '.groovy')
        }

        if(!routeFile.exists()) {
            throw new IllegalArgumentException("Route specified as dependency by $requirer ($routeName) does not exist in $routesDir")
        }
        
        log.info "Starting dependency $routeName required by $requirer"
        
        this.updateRoute(routeFile)
    }

    @CompileStatic
    private Logger configureRouteLogging(Route route) {

        Logger routeLog = Logger.getLogger("->$route.name<-")
        routeLog.getRootLogger().removeAllAppenders()

        if(!routeLog.getAppender('RouteConsole')) {
            ConsoleAppender consoleAppender = new ConsoleAppender()
            consoleAppender.name = 'RouteConsole'
            consoleAppender.setLayout(new PatternLayout("%d %p \t %t \t%m%n"))
            consoleAppender.setThreshold(Level.DEBUG)
            consoleAppender.setWriter(new PrintWriter(System.err))
            routeLog.addAppender(consoleAppender)
        }

        String perRouteLogs = System.properties
                                    .getOrDefault('camelpack.enablePerRouteLogging', System.getenv('CAMELPACK_ENABLE_PER_ROUTE_LOGGING')) 
                                    ?: 'false'

        if(perRouteLogs != 'true')
            return routeLog

        String logDirectoryPath = System.properties.getOrDefault('camelpack.routeLogDirectory', System.getenv('CAMELPACK_ROUTE_LOG_DIRECTORY')) ?: 'routes/logs'
        File logDirectory = new File(logDirectoryPath)
        if(!logDirectory.exists()) {
            log.info "Creating log directory $logDirectory as it does not exist"
            logDirectory.mkdirs()
        }
        
        if(routeLog.getAppender("FileLogger") == null) {
            FileAppender appender = new FileAppender();
            appender.setName("FileLogger");
            appender.setFile("$logDirectoryPath/${route.name}.log");
            appender.setLayout(new PatternLayout("%d %-5p %m%n"));
            appender.setThreshold(Level.DEBUG);
            appender.setAppend(true);
            appender.activateOptions();
            routeLog.addAppender(appender)
        }
        return routeLog
    }
    
   
    void pause(String routeName) {
        Route route = getRoute(routeName)
        if(route.state == 'active') {
            log.info 'Pause route: ' + routeName
            route.state = 'paused'
            route.lastUpdated = 0
            route.context.stop()
            route.context = null
        }
    }
    
    void unpause(String routeName) {
        Route route = getRoute(routeName)
        if(route.state == 'paused') {
            route.state = 'active'
            route.lastUpdated = 0
            setup(route)
        }
    }
    
    @Scheduled(fixedRate = "60s")
    void refreshRoutes() {
        setup()
    }
    
    @CompileStatic
    void applyComponentConfiguration(CamelContext context) {
        ConfigObject cfg = (ConfigObject)this.getConfig().getOrDefault('components', null)
        if(cfg.is(null)) {
            log.info "No custom component configuration found"
            return
        }
        cfg.each { componentName, attributes ->
            log.info "Configuring component $componentName with attributes $attributes"
            Component component = context.getComponent((String)componentName, true)
            Map<String,Object> attributeMap = (Map)attributes
            attributeMap.each { propertyName, propertyValue ->
                component[propertyName] = propertyValue
            }
        }
    }
    
    void showSimplifiedStacktraces() {

        Logger.getLogger('org.apache.camel.processor.DefaultErrorHandler').useParentHandlers = false

        org.apache.log4j.Logger.getLogger('org.apache.camel.processor.DefaultErrorHandler').setLevel(org.apache.log4j.Level.OFF)
        
//        try {
//            ((ch.qos.logback.classic.Logger)LoggerFactory.getLogger('org.apache.camel.processor.DefaultErrorHandler')).setLevel(ch.qos.logback.classic.Level.OFF)
//        }
//        catch(Throwable t) {
//            // Ignore
//        }
    }
}
